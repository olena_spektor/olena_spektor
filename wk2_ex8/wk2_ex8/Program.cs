﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk2_ex8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create an app that displays a true/false statement and gets the user's input
            //Based on the answer you need to give a user feedback
            // this feedbach needs to be stored in a variable and then display after the if statment

            Console.WriteLine("Sky is yellow.");
            Console.Write("Is is true? (true/false)...  ");
            bool ans = bool.Parse(Console.ReadLine());

            if (ans == true)
            {
                Console.WriteLine($"\nThe answer <{ans}> is wrong. The sky is blue.");
            }
            else
            {
                Console.WriteLine($"\nThe answer <{ans}> is correct. The sky is blue.");
            }

            Console.ReadLine();

        }
    }
}
