﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w2_ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            var option = "";

            Console.WriteLine("1. Convert KM to Mile.");
            Console.WriteLine("2. Convert Mile to KM.");
            Console.WriteLine();
            Console.Write("Please choose an option and enter the number here: ");
            option = Console.ReadLine();
            Console.WriteLine();

            int km;
            double a = 0.621371; // 1 km = 0.621371 miles
            int mile;
            double b = 1.609344; // 1 mile = 1.609344 km

            switch (option)
            {
                case "1":
                    Console.Clear();
                    Console.Write("How many KM do you want to convert? ");
                    km = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    Console.WriteLine($"{km} km = {km * a} miles");
                    Console.ReadLine();
                    break;

                case "2":
                    Console.Clear();
                    Console.Write("How many miles do you want to convert? ");
                    mile = int.Parse(Console.ReadLine());
                    Console.WriteLine();
                    Console.WriteLine($"{mile} mile(s) = {mile * b} km");
                    Console.ReadLine();
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine($"There is no the option {option}.");
                    Console.ReadLine();
                    break;

            }
        }
    }
}
