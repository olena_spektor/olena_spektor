﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w2_ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 3;
            var b = 5;

            if (a > b)
            {
                Console.WriteLine($"Number1 ({a}) is bigger than Number2 ({b}).");
            }
            else
            {
                Console.WriteLine($"Number1 ({a}) is smaller than Number2 ({b})");
            }
        }
    }
}
