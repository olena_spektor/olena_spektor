﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk2_ex7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create an app that receives user input and convert that into a number.
            //This number will be your index so that it can produce a different result in your app.
            //Set the counter to 20 so that you see the effect of a do while app, even if the number is bigger 20.

            int count = 20;
            int i = 0;

            Console.Write("Enter your number: ");
            i = int.Parse(Console.ReadLine());

            do
            {
                Console.WriteLine($"Line number {i++}");
            } while (i <= count);

            Console.ReadLine();

        }
    }
}
