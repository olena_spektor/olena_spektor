﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace w2_ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            var ans = true;
            int score = 0;

            Console.WriteLine("1. Horse is a small animal. (true/false)");
            ans = bool.Parse(Console.ReadLine());
            if(ans)
            {
                Console.WriteLine($"Your score is {score} of 5.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            score++;
            Console.WriteLine();

            Console.WriteLine("2. Bongard Centre is located in Welcome Bay. (true/false)");
            ans = bool.Parse(Console.ReadLine());
            if(ans)
            {
                Console.WriteLine($"Your score is {score} of 5.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            score++;
            Console.WriteLine();


            Console.WriteLine("3. Coffeine belongs to drugs. (true/false)");
            ans = bool.Parse(Console.ReadLine());
            if(ans != true)
            {
                Console.WriteLine($"Your score is {score} of 5.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            score++;
            Console.WriteLine();

            Console.WriteLine("4. Dubai is hot in July. (true/false)");
            ans = bool.Parse(Console.ReadLine());
            if(ans != true)
            {
                Console.WriteLine($"Your score is {score} of 5.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            score++;
            Console.WriteLine();

            Console.WriteLine("5. Sky is yellow. (true/false)");
            ans = bool.Parse(Console.ReadLine());
            if(ans)
            {
                Console.WriteLine($"Your score is {score} of 5.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            score++;
            Console.WriteLine();

            Console.WriteLine($"Your score is {score} of 5.");
            Console.ReadLine();
        }
    }
}
