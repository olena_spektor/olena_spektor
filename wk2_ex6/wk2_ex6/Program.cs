﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk2_ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            // print the same line several times, but in each line we want to display the number of the iteration
            // use a while loop

            int count = 1;
            while (count <= 10)
            {
                Console.WriteLine($"Line number {count++}");
            }
        }
    }
}
